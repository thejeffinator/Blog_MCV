<?php session_start();

require_once("../classes/db/QueryManager.php");
require_once("../classes/model/User.php");

$q = new Querymanager();

/***************************************************
 * Save new user
 ***************************************************/
if (isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['phonenumber'])
    && isset($_POST['password']) && ($_POST['action'] == 'saveUser')
) {
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $phonenumber = $_POST['phonenumber'];
    $password = md5($_POST['password'].$firstname);
    $q->saveUser($firstname,$lastname,$phonenumber,$password);
    header('Location: ../view/all_users.php');
}

/***************************************************
 * User login:
 ***************************************************/
if (isset($_POST['firstname']) && isset($_POST['password']) && ($_POST['action'] == 'login')) {
    $firstname = $_POST['firstname'];
    $password = md5($_POST['password'].$firstname);
    $login = $q->loginUser($firstname, $password);

    $user = $q->findUserByName($firstname, $password);
    $_SESSION['login'] = serialize($login);
    $_SESSION['name'] = $firstname;
    $_SESSION['id'] = $user->getId();
    $_SESSION['password'] = $password;
    header('Location: ../view/login.php');
}

/***************************************************
 * User Logout:
 ***************************************************/
if (($_GET['submit'] == "ja") && ($_GET['action'] == 'logout')) {
    $_SESSION['admin'] = 0;
    session_destroy();
    header('Location: ../view/homepage.php');
}

if (($_GET['action'] == "logout") && ($_GET['submit'] == "nee")) {
    header('Location: ../view/homepage.php');
}

/***************************************************
 * Find users:
 ***************************************************/

if (($_GET['action'] == "findAllUsers")) {
    $userList = $q->findAllUsers();
    $_SESSION['userList'] = serialize($userList);
    header('Location: ../view/all_users.php');
}

if (isset($_GET['id']) && ($_GET['action'] == 'findUserById')) {
    $id = $_GET['id'];
    $user = $q->findUserById($id);
    $_SESSION['user'] = serialize($user);
    header('Location: ../view/one_user.php');


}

/***************************************************
 * Delete user:
 ***************************************************/
if (isset($_GET['id'])&&($_GET['action']=='deleteUserById')) {
    $id = $_GET['id'];
    $user = $q->findUserById($id);
    $_SESSION['user'] = serialize($user);
    header('Location: ../view/delete_user.php?id='.$id);

}


if (isset($_GET['id']) && ($_GET['action'] == 'JA')) {
    $id = $_GET['id'];
    $user = $q->deleteUser($id);
    header('Location: ../index.php');


}
if (isset($_GET['id']) && ($_GET['action'] == 'NEE')) {
    header('Location: ../index.php');


}

/***************************************************
 * Update User:
 ***************************************************/

if (isset($_POST['id']) && ($_POST['action'] == 'update')) {
    $id = $_POST['id'];
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $phonenumber = $_POST['phonenumber'];
    $password = md5($_POST['password'].$firstname);
    $user = $q->updateUser($id,$firstname,$lastname,$phonenumber,$password);
    header('Location: ../index.php');

}
if (isset($_GET['id']) && ($_GET['action'] == 'updateForm')) {
    $id = $_GET['id'];
    $user = $q->findUserById($id);
    $_SESSION['user'] = serialize($user);
    header('Location: ../view/update_user.php');
}
?>


</body>
</html>