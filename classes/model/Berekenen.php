<?php

/**
 * Created by PhpStorm.
 * User: Jeffrey
 * Date: 24-Mar-17
 * Time: 15:05
 */
class Berekenen
{
    private $getal1;
    private $getal2;

    public function __construct()
    {
    }

    public function berekenPlus($getal1,$getal2){
        return $getal1 + $getal2;
    }

    public function berekenMin($getal1,$getal2){
        return $getal1 - $getal2;
    }

    public function berekenDelen($getal1,$getal2){
        return $getal1 / $getal2;
    }

    public function berekenMaal($getal1,$getal2){
        return $getal1 * $getal2;
    }

}