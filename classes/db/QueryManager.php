<?php

require_once("MySQLConnection.php");
require_once("../classes/model/User.php");
require_once("../classes/model/Message.php");


class QueryManager
{

    private $dbconn;

    public function QueryManager()
    {
        // OOP: instantieer een MySQLConnection-object en geef deze als resultaat
        $this->dbconn = new MySQLConnection();
    }

    /***************************************************
     * Hieronder staan alle queries voor de users:
     ***************************************************/

    public function findUserByName($firstname, $password)
    {

        // 1 rij uit de database
        $result = $this->dbconn->query("SELECT * FROM user WHERE firstname ='$firstname' AND password = '$password'");
        $row = mysqli_fetch_array($result);
        // OOP: instantieer een Userobject en geef deze als resultaat
        return (new User($row['id'], $row['firstname'], $row['lastname'], $row['phonenumber'], $row['password']));
    }

    public function findUserById($id)
    {
        // 1 rij uit de database
        $result = $this->dbconn->query("SELECT * FROM user WHERE id='$id'");
        $row = mysqli_fetch_array($result);
        // OOP: instantieer een Userobject en geef deze als resultaat
        return (new User($row['id'], $row['firstname'], $row['lastname'], $row['phonenumber'], $row['password']));
    }

    // alle users
    public function findAllUsers()
    {
        $result = $this->dbconn->query("SELECT * FROM user");
        // OOP: alle users:
        while ($row = mysqli_fetch_array($result)) {
            $userList[] = new User($row['id'], $row['firstname'], $row['lastname'], $row['phonenumber'], $row['password']);
        }
        return $userList;
    }

    public function saveUser($firstname, $lastname, $phonenumber, $password)
    {
        $this->dbconn->query("INSERT into user (firstname, lastname, phonenumber, password) VALUES 
	    ('$firstname','$lastname','$phonenumber', '$password')");
    }

    public function updateUser($id, $firstname, $lastname, $phonenumber, $password)
    {
        $this->dbconn->query("UPDATE user SET firstname='$firstname', lastname='$lastname', phonenumber='$phonenumber', password='$password' WHERE id='$id'");
    }

    public function deleteUser($id)
    {
        $this->dbconn->query("DELETE FROM user WHERE id='$id'");
    }

    public function loginUser($firstname, $password)
    {  //checkuser
        $result = $this->dbconn->query("SELECT * FROM user WHERE firstname ='$firstname' AND password = '$password'");
        $row = mysqli_num_rows($result);
        return $row;
    }

    /***************************************************
     * Hieronder staan alle queries voor de berichten:
     ***************************************************/

    public function findMessageById($id)
    {
        
        // Haal een rij uit de database
        $result = $this->dbconn->query("SELECT * FROM message WHERE id='$id'");
        $row = mysqli_fetch_array($result);
        return (new Message($row['id'], $row['title'], $row['content']));
    }

    public function findAllMessages()
    {
        $result = $this->dbconn->query("SELECT * FROM message");

        while ($row = mysqli_fetch_array($result)) {
            $messageList[] = new Message($row['id'], $row['title'], $row['content']);
        }
        return $messageList;

    }

    public function saveMessage($title, $content, $userid)
    {
        $this->dbconn->query("INSERT INTO message (title, content, userid) VALUES ('$title', '$content', $userid)");
    }

    public function updateMessage($id, $title, $content)
    {
        $this->dbconn->query("UPDATE message SET title='$title',content='$content' WHERE id='$id'");
    }

    public function deleteMessage($id)
    {
        $this->dbconn->query("DELETE from message WHERE id='$id'");
    }


}
