<?php session_start();?>
<?php require_once("../classes/model/Berekenen.php"); ?>
<?php

$r = new Berekenen();
$result = Null;
if (!empty($_POST['action'])) {
    $getal1 = $_POST['getal1'];
    $getal2 = $_POST['getal2'];
    $action = $_POST['action'];
    $action = $_POST['action'];

    if ($action == "min") {
        $result = $r->berekenMin($getal1, $getal2);
    }

    if ($action == "plus") {
        $result = $r->berekenPlus($getal1, $getal2);
    }

    if ($action == "delen") {
        $result = $r->berekenDelen($getal1, $getal2);
    }

    if ($action == "maal") {
        $result = $r->berekenMaal($getal1, $getal2);
    }

}
?>
<?php include '../includes/top.php'; ?>
<body>
<?php include '../includes/header.php'; ?>
<?php include '../includes/menu.php'; ?>
<div id="content" class="mdl-layout__content col-md-9">

    <div class="mdl-grid">
        <div class="mdl-cell mdl-cell--3-offset-desktop mdl-cell--6-col mdl-cell--4-col-phone">
            <h2>De zelf gemaakte rekenmachine</h2>
            <form method="post">
                <input name="getal1" type="number" placeholder="Getal 1" value=""/>
                <input name="getal2" type="number" placeholder="Getal 2" value=""/>
                <input name="action" type="submit" value="plus"/>
                <input name="action" type="submit" value="min"/>
                <input name="action" type="submit" value="delen"/>
                <input name="action" type="submit" value="maal"/>


            </form>
            <br>

            <?php if (isset($result)) {
                echo 'De uitkomst is ' . $result;
            } ?>
        </div>
    </div>

</div>
</body>
</html>