<?php session_start(); ?>
<?php include '../includes/top.php'; ?>
<body>
<?php include '../includes/header.php'; ?>
<?php include '../includes/menu.php'; ?>
<div id="content" class="mdl-layout__content col-md-9">
    <div class="mdl-cell mdl-cell--2-offset-desktop mdl-cell--8-col mdl-cell--4-col-phone">

        <h2>Users</h2>
        <?php

        require_once("../classes/model/User.php");

        if (isset($_SESSION['userList'])) {

            $userList = unserialize($_SESSION['userList']);
            echo '<table class="user">';
            echo '<tr>
            <th>id</th>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Phonenumber</th>
            </tr>';
            foreach ($userList as $user) {
                echo '<tr>';
                echo '<td>' . $user->getId() . '</td>
                  <td>' . $user->getFirstName() . '</td>
                  <td>' . $user->getLastName() . '</td>
                  <td>' . $user->getPhonenumber() . '</td>';
                echo "<td class='picture'><a  href='../controller/userController.php?id=" . $user->getId() . "&&action=deleteUserById'>
					<img src=\"images/delete.png\" width=\"50\" height=\"50\"></a></td>";
                echo " <td class='picture'><a href='../controller/userController.php?id=" . $user->getId() . "&&action=updateForm'>
					<img src=\"images/edit.png\" width=\"50\" height=\"50\"></a></td>";
                echo " <td class='picture'><a href='../controller/userController.php?id=" . $user->getId() . "&&action=findUserById'>
					<img src=\"images/select.png\" width=\"50\" height=\"50\"></a></td>";
                echo '</tr>';
            }
            echo '<table>';
        }


        ?>
    </div>
</div>
</body>
</html>