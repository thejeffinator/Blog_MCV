<?php include '../includes/sentry.php'; ?>
<?php include '../includes/top.php'; ?>
<body>
<?php include '../includes/header.php'; ?>
<?php include '../includes/menu.php'; ?>
<div id="content" class="mdl-layout__content col-md-9">
    <div class="mdl-cell mdl-cell--2-offset-desktop mdl-cell--8-col mdl-cell--4-col-phone">
        <form action="../controller/messageController.php" method="post">
            <?php require_once("../classes/model/Message.php");
            $message = unserialize($_SESSION['message']);
            ?>
            <h2>Bericht</h2>
            <br><br>

            <input type="hidden" name="id" value="<?php echo $message->getId(); ?>">
            <input type="hidden" name="action" value="update">
            Titel:<br><input name="title" type="text"
                             value="<?php echo $message->getTitle(); ?>"/><br/>
            <textarea name="content" style="width:400px; height:300px;" placeholder="Bericht"><?php
                echo $message->getContent(); ?></textarea> <br>
            <button class="submit" type="submit">Bericht opslaan</button>
        </form>
    </div>
</div>
</body>
</html>