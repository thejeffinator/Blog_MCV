<?php include '../includes/sentry.php'; ?>
<?php include '../includes/top.php'; ?>
<body>
<?php include '../includes/header.php'; ?>
<?php include '../includes/menu.php'; ?>
<div id="content" class="mdl-layout__content col-md-9">
    <div class="mdl-cell mdl-cell--2-offset-desktop mdl-cell--8-col mdl-cell--4-col-phone">
        <form action="../controller/userController.php" method="GET">
            <h2>Weet u zeker dat u wilt uitloggen?</h2>
            <br><br>

            <p>
                <input type="hidden" name="action" value="logout">
                <button type="submit" name="submit" value="ja">Ja</button>
                <button type="submit" name="submit" value="nee">Nee</button>
            </p>


        </form>

    </div>
</div>
</body>
</html>