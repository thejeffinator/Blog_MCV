<?php include '../includes/sentry.php'; ?>
<?php include '../includes/top.php'; ?>

<body>
<?php include '../includes/header.php'; ?>
<?php include '../includes/menu.php'; ?>
<div id="content" class="mdl-layout__content col-md-9">
    <div class="mdl-cell mdl-cell--2-offset-desktop mdl-cell--8-col mdl-cell--4-col-phone">
        <table>
            <tr>
                <th>Plaatje:</th>
                <th>Plaatje weghalen:</th>
            </tr>
            <?php
            if ($handle = opendir('upload')) {
                while (false !== ($file = readdir($handle))) {
                    if ($file != "." && $file != ".." && $file != "file.php" && $file != "../controller/upload_images.php") {
                        echo '<tr><td><a href="upload/' . $file . '">' . "<img src=\"upload/$file\" width=\"50\" height=\"60\"/>" . '</a></td><td><a href=\'../view/delete_file_form.php?file=' . $file . '\'><img src="images/delete.png" width="50" height="50"/></a></tr>';
                    }
                }
                closedir($handle);
            }
            ?>
        </table>
        <br/>
        <form enctype="multipart/form-data" action="../controller/fileController.php" method="POST">
            Kies een bestand: <input class="submit" name="uploaded" type="file"/><br/><br/>
            <button class="submit" type="submit">Uploaden!</button>
        </form>
    </div>
</div>
</body>
</html>
