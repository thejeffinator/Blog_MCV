<?php session_start(); ?>
<?php include '../includes/top.php'; ?>
<body>
<?php include '../includes/header.php'; ?>
<?php include '../includes/menu.php'; ?>
<div id="content" class="mdl-layout__content col-md-9">
    <div class="mdl-cell mdl-cell--2-offset-desktop mdl-cell--8-col mdl-cell--4-col-phone">

        <h2>Bericht</h2>

        <?php

        require_once("../classes/model/Message.php");

        $message = unserialize($_SESSION['message']);

        echo "<br/><h4><font color=\"blue\">" . $message->getTitle() . "</font></h4>";
        echo $message->getContent();
        echo "<br/>";
        echo "<input type=\"hidden\" name=\"id\" value=\"" . $message->getId() . "\"";
        echo '</br><br><br>'; // extra lege regel */
        ?>


    </div>
</div>
</body>
</html>