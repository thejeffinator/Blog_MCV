<?php session_start();?>
<!DOCTYPE html>
<html>
<head>
    <title>Blog</title>
    <link type='text/css' rel='stylesheet' href='/css/<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/config.php'); echo $thema; ?>.css'/>

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>

</head>
<body>
<?php include '../../includes/header.php'; ?>
<?php include '../../includes/menu.php'; ?>

<?php include '../../classes/model/Persoon.php';
include '../../classes/model/Docent.php';
include '../../classes/model/Student.php';
?>

<div id="content" class="mdl-layout__content col-md-9">

    <div class="mdl-grid">
        <div class="mdl-cell mdl-cell--3-offset-desktop mdl-cell--6-col mdl-cell--4-col-phone">
            <h2>Een Docent</h2>
            <?php
            $docent = unserialize($_SESSION['docent']);
            echo $docent->getUser()." Uw kamer nummer is ". $docent->getKamernummer('7.24');

            ?>

        </div>
    </div>

</div>
</body>
</html>