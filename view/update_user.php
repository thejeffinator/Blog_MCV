<?php include '../includes/sentry.php'; ?>
<?php include '../includes/top.php'; ?>
<body>
<?php include '../includes/header.php'; ?>
<?php include '../includes/menu.php'; ?>
<div id="content" class="mdl-layout__content col-md-9">
    <div class="mdl-cell mdl-cell--2-offset-desktop mdl-cell--8-col mdl-cell--4-col-phone">
        <form action="../controller/UserController.php" method="post">
            <?php require_once("../classes/model/User.php");
            $user = unserialize($_SESSION['user']);
            ?>
            <h2>Update user</h2>
            <br><br>

            <input type="hidden" name="id" value="<?php echo $user->getId(); ?>">
            <input type="hidden" name="action" value="update">
            Firstname:<br> <input name="firstname" type="text" value="<?php echo $user->getFirstname(); ?>"
                                  required=""/><br/>
            Lastname:<br> <input name="lastname" type="text" value="<?php echo $user->getLastName(); ?>"/><br/>
            Phonenumber:<br> <input name="phonenumber" type="text" value="<?php echo $user->getPhonenumber(); ?>"/><br/>
            Password:<br> <input name="password" type="password" value="" required=""/><br/>
            <button class="submit" type="submit">Gebruiker updaten</button>
        </form>
    </div>
</div>
</body>
</html>