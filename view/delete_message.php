<?php include '../includes/sentry.php'; ?>
<?php include '../includes/top.php'; ?>
<body>
<?php include '../includes/header.php'; ?>
<?php include '../includes/menu.php'; ?>
<div id="content" class="mdl-layout__content col-md-9">
    <form action="../controller/messageController.php" method="GET">
        <div class="mdl-cell mdl-cell--2-offset-desktop mdl-cell--8-col mdl-cell--4-col-phone">
            <?php require_once("../classes/model/Message.php");
            $message = unserialize($_SESSION['message']);
            ?>
            <h2>Wilt u dit bericht verwijderen</h2>
            <br><br>

            <b>Titel:</b> <?php echo $message->getTitle(); ?><br>
            <b>Bericht:</b> <?php echo $message->getContent(); ?> <br>

            <p>
                <input type="hidden" name="id" value="<?php echo $message->getId(); ?>">
                <button type="submit" name="action" value="JA">Ja</button>
                <button type="submit" name="action" value="NEE">Nee</button>
            </p>


    </form>
</div>
</div>
</body>
</html>