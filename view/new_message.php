<?php session_start(); ?>
<?php include '../includes/top.php'; ?>

<body>
<?php include '../includes/header.php'; ?>
<?php include '../includes/menu.php'; ?>

<div id="content" class="mdl- col-md-9">
    <div class="mdl-cell mdl-cell--2-offset-desktop mdl-cell--8-col mdl-cell--4-col-phone">
        <h3>Nieuw bericht: </h3>
        <br><br>

        <form method="post" action='../controller/messageController.php'>
            <input name="action" type="hidden" value="saveMessage"/>
            Titel:<br><input name="title" type="text" placeholder="Titel" required/><br/><br>Bericht:<br/>
            <textarea name="content" style="width:400px; height:300px;" placeholder="Bericht" required></textarea>
            <br>
            <button class="submit" type="submit">Bericht opslaan</button>
        </form>
    </div>
</div>

</body>
</html>