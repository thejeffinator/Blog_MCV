<!DOCTYPE html>
<html>
<head>
    <title>Blog</title>
    <link type='text/css' rel='stylesheet' href='/css/<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/config.php'); echo $thema; ?>.css'/>

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>