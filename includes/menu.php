<?php

$menu_admin = "
        <span><a class='mdl-navigation__link menuitem' href='/view/homepage.php'>Home</a></span>
        <span><a class='mdl-navigation__link menuitem' href='/controller/messageController.php?action=findAllMessages'>Overzicht berichten</a></span>
        <span><a class='mdl-navigation__link menuitem' href='/view/new_message.php'>Bericht schrijven</a></span>
        <span><a class='mdl-navigation__link menuitem' href='/controller/userController.php?action=findAllUsers'>Overzicht users</a></span>
        <span><a class='mdl-navigation__link menuitem' href='/view/new_user.php'>User toevoegen</a></span>
        <span><a class='mdl-navigation__link menuitem' href='/view/upload_file_form.php'>Bestand uploaden</a></span>
        <span><a class='mdl-navigation__link menuitem' href='/view/admin_config.php'>Selecteer thema</a></span>
        <span><a class='mdl-navigation__link menuitem' href='/view/rekenmachine.php'>Rekenmachine</a></span>
        <span><a class='mdl-navigation__link menuitem' href='/view/inheritance/index.php'>Inheritance</a></span>
        <span><a class='mdl-navigation__link menuitem' href='/view/logout_user_form.php'>Uitloggen</a></span>";
$menu_noadmin =
    "
        <span><a class='mdl-navigation__link menuitem' href='/view/homepage.php'>Home</a></span>
        <span><a class='mdl-navigation__link menuitem' href='/controller/messageController.php?action=findAllMessages'>Overzicht berichten</a></span>
        <span><a class='mdl-navigation__link menuitem' href='/view/new_message.php'>Bericht schrijven</a></span>
        <span><a class='mdl-navigation__link menuitem' href='/view/admin_config.php'>Selecteer thema</a></span>
        <span><a class='mdl-navigation__link menuitem' href='/view/rekenmachine.php'>Rekenmachine</a></span>
        <span><a class='mdl-navigation__link menuitem' href='/view/inheritance/index.php'>Inheritance</a></span>
        <span><a class='mdl-navigation__link menuitem' href='/view/login_user_form.php'>Inloggen</a></span>
    ";

$title = "Blog S1102648 OOP";

if ($thema == "stylesheet1") {
    echo "<div id='menu'>";
    error_reporting(0);
    if ($_SESSION['admin'] == 2) {
        echo
            "<ul>
        <h2><b>Menu</b></h2>
        <br />
        " . $menu_admin . "
        <br />";
        echo 'Welkom ' . $_SESSION['name'] . '<br>';
    }
    if ($_SESSION['admin'] == 0) {
        echo
            "
        <h2> <b>Menu</b></h2>
        <ul class='menulist'>
        
        " . $menu_noadmin . "
        <br /> 
        </ul>";
    }
    echo '</div>';

} else if ($thema == "stylesheet2") { ?>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header ">
    <header class="mdl-layout__header">
        <div class="mdl-layout__header-row">
            <!-- Title -->
            <span class="mdl-layout-title"><?php echo $title;?></span>
            <!-- Add spacer, to align navigation to the right -->
            <div class="mdl-layout-spacer"></div>

        </div>
    </header>
    <div class="mdl-layout__drawer ">
        <span class="mdl-layout-title">Menu</span>
        <nav class="mdl-navigation">
            <?PHP error_reporting(0);
            if ($_SESSION['admin'] == 2) {
                echo $menu_admin;
            }
            if ($_SESSION['admin'] == 0) {
                echo ".$menu_noadmin.";
            } ?>
        </nav>
    </div>
    <?php } else if ($thema == "stylesheet3"){ ?>
    <div class="container-fluid">
        <div class="row content">
            <div class="col-sm-3 sidenav">
                <h4><?php echo $title;?></h4>
                <ul class="nav nav-pills nav-stacked">
                    <?PHP error_reporting(0);
                    if ($_SESSION['admin'] == 2) {
                        echo $menu_admin;
                    }
                    if ($_SESSION['admin'] == 0) {
                        echo ".$menu_noadmin.";
                    } ?>
                </ul>
            </div>
            <?php } ?>
